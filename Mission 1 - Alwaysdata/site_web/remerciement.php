<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Remerciements</title>
  <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>

  <div class="container">
    <h1>Merci pour votre participation !</h1>
    <p>Nous avons bien reçu vos informations. Vous serez contacté bientôt avec plus de détails concernant la remise de diplôme.</p>

    <a href="index.html" class="redirect-button">Retour à l'accueil</a>
  </div>

</body>
</html>
