<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $nom = $_POST["nom"];
    $prenom = $_POST["prenom"];
    $annee_diplome = $_POST["annee_diplome"];
    $specialite = $_POST["specialite"];
    $email = $_POST["email"];
    $telephone = $_POST["telephone"];

    // Connexion à la base de données 
    $serveur = "";
    $utilisateur = ""; 
    $mot_de_passe = ""; 
    $nom_base_de_donnees = "";

    $connexion = new mysqli($serveur, $utilisateur, $mot_de_passe, $nom_base_de_donnees);

    // Vérification de la connexion
    if ($connexion->connect_error) {
    die("Erreur de connexion à la base de données : " . $connexion->connect_error);
    }

    // Préparation de la requête d'insertion
    $requete = $connexion->prepare("INSERT INTO eleves (nom, prenom, annee_diplome, specialite, email, telephone) VALUES (?, ?, ?, ?, ?, ?)");

    // Liaison des paramètres
    $requete->bind_param("ssisss", $nom, $prenom, $annee_diplome, $specialite, $email, $telephone);

    // Exécution de la requête
    $requete->execute();

    // Fermeture de la requête et de la connexion
    $requete->close();
    $connexion->close();

    // Réponse personnalisée pour l'utilisateur
    $message = "Merci, $prenom $nom, pour avoir partagé vos informations ! Nous sommes ravis de vous inviter à la remise de diplôme de l'année $annee_diplome. Vous recevrez bientôt plus d'informations par email.";
        
    // Redirection vers une page de remerciement
    header("Location: remerciement.php");

} else {
    // Redirection si le formulaire n'est pas soumis
    header("Location: index.html");
}
?>
